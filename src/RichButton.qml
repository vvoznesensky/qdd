import QtQuick 2.12
import QtQuick.Shapes 1.14 as Shapes
import org.kde.kirigami 2.5 as Kirigami

Row {
    id: root
    property string symbol
    property int numerator
    property int denominator
    signal tapped()
    property bool chosen
    width: parent.width

    Text {
        id: symbolItem
        //anchors {left: parent.left; top: parent.top}
        text: root.symbol
        font.pointSize: Kirigami.Units.gridUnit
        style: root.chosen? Text.Outline: Text.Normal
        styleColor: "green"
        //width: 2 * Kirigami.Units.gridUnit // minimumPixelSize
    }
    Column {
        width: parent.width - symbolItem.width
        //anchors {left: symbolItem.right; right: parent.right}
        
        Text {
            id: numText
            anchors {left: parent.left}
            text: root.numerator
            color: 'green'
            font.bold: chosen
        }
        Text {
            id: denText
            anchors {left: parent.left}
            text: root.denominator
            font.bold: chosen
        }
    }
    TapHandler {
        id: tapHandler
        onTapped: {
            root.tapped()
            print(root.symbol + " tapped")
        }
    }
}
