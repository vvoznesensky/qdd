import QtQuick 2.12

ListModel {
    id: root

    ListElement {
        face: "👧"
        headline: "Заставить московское правительство внедрить ПД в столовых \
бюджетных школ"
        text: '\
К сожалению, младшая школа, ходящая на продлёнку, не может голосовать ногами \
против школьных обедов, поставляемых по минимальной цене единственным \
монополистом.<br>
Предлагается обязать московские власти установить в школьных столовых пульты \
обратной связи, авторизуемую карточкой учащегося.<br>
На пульте 5 рядов кнопок: "Салат", "Суп", "Второе", "Напиток", "Отправить".
Первые 4 ряда имеют по три кнопки, "Хорошо", "Подозрительно" и "Не смог есть". \
5-й ряд имеет кнопку "Отправить".<br>
Результаты записывать ну не в блокчейн, но в публичную базу данных. \
За несъедобную еду безусловно штрафовать, подозрительную еду проверять \
компетентным органом.
'
        cur_likes: 40; all_likes: 80
        cur_not_ready: 10; all_not_ready: 20
        cur_dislikes: 50; all_dislikes: 100
        cur_trash_bins: 25; all_trash_bins: 50
        state: "discuss"
    }

    ListElement {
        face: "🎅"
        headline: "Даёшь глобальное похолодание!"
        text: "\
Это ваше глобальное потепление мешает мне путешествовать.<br>
Я банально не могу выехать из своей Лапландии на юг, ведь мне и моим оленям \
становится жарко. Это опасно для наших жизней!<br>
Нужно вернуть естественный порядок вещей, запретив нагревать атмосферу."
        cur_likes: 2; all_likes: 5
        cur_not_ready: 2; all_not_ready: 4
        cur_dislikes: 602; all_dislikes: 1204
        cur_trash_bins: 7; all_trash_bins: 14
        state: "discuss"
    }

    ListElement {
        face: "💂"
        headline: '<u><font color="darkred"><s>\
Отжать</s></font> <font color="green">Собрать</font> все \
<font color="darkred"><s>бабки</s></font> <font color="green">деньги</font></u>\
 и отдать Её Величеству'
        text: "Наша королева Елизавета II - выше всяких похвал.<br> \
Благодаря монархии только мы, англичане, понимаем \
по-настоящему, что значит нести бремя белого человека, поэтому \
ваши денюшки будут сохраннее в нашем британском банке."
        cur_likes: 28; all_likes: 57
        cur_not_ready: 14; all_not_ready: 28
        cur_dislikes: 17 ; all_dislikes: 34 
        cur_trash_bins: 108; all_trash_bins: 217
        state: "discuss"
    }

    ListElement {
        face: "🧛"
        headline: "Кончайте уже дома по ночам прятаться"
        text: "Дурная привычка обывателей спать по ночам оставляет нас \
голодными. Пожалейте вампиров, ведь солнечный свет смертельно \
опасен для нас, а обычная людская пища нами не усваивается."
        cur_likes: 6; all_likes: 12
        cur_not_ready: 0; all_not_ready: 0
        cur_dislikes: 363; all_dislikes: 726
        cur_trash_bins: 7; all_trash_bins: 15
        state: "discuss"
    }

    ListElement {
        face: "🤶"
        headline: 'Оставить <font color="darkred"><s>старого развратника\
</font></s> Санту дома'
        text: 'Это только кажется, что наш дорогой Санта-Клаус ещё ого-го.<br>
<s><font color="darkred">Дома должен сидеть старый <u>чёрт</u></font></s>\
<font color="green">Дедушка старенький, ему нужно сидеть дома</s></font> \
<s><font color="darkred">, а не шляться неизвестно где неизвестно с кем</font>\
</s>.'
        cur_likes: 6; all_likes: 13
        cur_not_ready: 3; all_not_ready: 7
        cur_dislikes: 55; all_dislikes: 110
        cur_trash_bins: 28; all_trash_bins: 56
        state: "discuss"
    }

    ListElement {
        face: "🤱"
        headline: "Чтобы всё время было лето и дети не болели"
        text: "Как можно быть против?"
        cur_likes: 512; all_likes: 1024
        cur_not_ready: 256; all_not_ready: 512
        cur_dislikes: 128; all_dislikes: 256
        cur_trash_bins: 1024; all_trash_bins: 2048
        state: "discuss"
    }

    ListElement {
        face: "🧛"
        headline: "Кончайте уже дома по ночам прятаться"
        text: "Дурная привычка обывателей спать по ночам оставляет нас \
голодными. Пожалейте вампиров, ведь солнечный свет смертельно \
опасен для нас, а обычная людская пища нами не усваивается."
        cur_likes: 6; all_likes: 12
        cur_not_ready: 0; all_not_ready: 0
        cur_dislikes: 363; all_dislikes: 726
        cur_trash_bins: 7; all_trash_bins: 15
        state: "ballots"
    }

    ListElement {
        face: "🤶"
        headline: 'Оставить <font color="darkred"><s>старого развратника\
</font></s> Санту дома'
        text: 'Это только кажется, что наш дорогой Санта-Клаус ещё ого-го.<br>
<s><font color="darkred">Дома должен сидеть старый <u>чёрт</u></font></s>\
<font color="green">Дедушка старенький, ему нужно сидеть дома</s></font> \
<s><font color="darkred">, а не шляться неизвестно где неизвестно с кем</font>\
</s>.'
        cur_likes: 6; all_likes: 13
        cur_not_ready: 3; all_not_ready: 7
        cur_dislikes: 55; all_dislikes: 110
        cur_trash_bins: 28; all_trash_bins: 56
        state: "results"
    }
}
