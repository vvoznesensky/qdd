import QtQuick 2.0
import QtQuick.Controls 2.0 as Controls
import org.kde.kirigami 2.5 as Kirigami

Kirigami.ApplicationWindow
{
    id: root
    title: qsTr("QML Direct Democracy")
    /*globalDrawer: Kirigami.GlobalDrawer {
        title: qsTr("QML Direct Democracy")
        titleIcon: "applications-graphics"
        actions: [
            Kirigami.Action {
                text: "View"
                icon.name: "view-list-icons"
                Kirigami.Action {
                    text: "View Action 1"
                    onTriggered: showPassiveNotification(i18n("VA 1 clicked"))
                }
                Kirigami.Action {
                    text: "View Action 2"
                    onTriggered: showPassiveNotification(i18n("VA 2 clicked"))
                }
            },
            Kirigami.Action {
                text: "Action 1"
                onTriggered: showPassiveNotification(i18n("Action 1 clicked"))
            },
            Kirigami.Action {
                text: "Action 2"
                onTriggered: showPassiveNotification(i18n("Action 2 clicked"))
            }
        ]
    }*/

    contextDrawer: Kirigami.ContextDrawer {
        id: contextDrawer
    }

    pageStack.initialPage: Qt.resolvedUrl("StartPage.qml")
}
