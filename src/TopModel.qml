import QtQuick 2.12

ListModel {
    ListElement {
        title: "💬Обсуждение"
        subModel: "MockRandomInitiatives"
    }
    ListElement {
        title: "🗳️Голосование"
        subModel: "MockRandomInitiatives"
    }
    ListElement {
        title: "📊Результаты"
        subModel: "MockRandomInitiatives"
    }
}
