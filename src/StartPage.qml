import QtQuick 2.14
import QtQuick.Layouts 1.14 as Layouts
import QtQuick.Controls 2.14 as Controls
import org.kde.kirigami 2.14 as Kirigami
import org.kde.kitemmodels 1.0 as KItemModels

Kirigami.ScrollablePage {
    titleDelegate: Kirigami.SearchField {
        id: searchField
        KeyNavigation.tab: (listView.currentIndex >= 0?
            listView.currentItem: listView)
        onTextChanged: {
            sortFilterModel.filterString = text
        }
        Layouts.Layout.fillHeight: true
        Layouts.Layout.fillWidth: true
        Layouts.Layout.topMargin: Kirigami.Units.smallSpacing
        Layouts.Layout.bottomMargin: Kirigami.Units.smallSpacing
    }
    ListView {
        id: listView
        property int currentIndex: -1
        property var opened: {
            "discuss": false,
            "ballots": false,
            "results": false}
        function refreshVisible() {
            for (var i = 0; i < count; i++) {
                var item = listView.itemAtIndex(i)
                print(item.element.face + item.visible + item.element.state)
                item.visible = opened[item.element.state]
                item.height = item.visible? i == currentIndex? 245: 68: 0
            }
        }
        width: parent.width
        height: parent.height

        Kirigami.PlaceholderMessage {
            anchors.centerIn: parent
            width: parent.width - (Kirigami.Units.largeSpacing * 4)
            visible: listView.count == 0
            text: "Инициативы не загружены в приложение"
            helpfulAction: Kirigami.Action {
                text: "Настроить приложение"
            }
        }

        model: KItemModels.KSortFilterProxyModel {
            id: sortFilterModel
            filterRole: "text"
            sourceModel: MockRandomInitiatives {}
        }
        
        signal sectionTapped(string sectionState)
        section {
            property: "state"
            criteria: ViewSection.FullString
            delegate: SectionHeading {
                id: sectionHeading
                open: listView.opened[section]
                Component.onCompleted: {
                    sectionTapped.connect(listView.sectionTapped)
                }
            }
            //labelPositioning: ViewSection.CurrentLabelAtStart
        }
        onSectionTapped: {
            listView.opened[sectionState] =
                !listView.opened[sectionState]
            listView.refreshVisible()
            print("Tapped " + sectionState)
        }

        delegate: Initiative {
            id: initiative
            element: model
            state: model.state
            visible: listView.section == model.state
            width: parent.width
            height: 0
            current: listView.currentIndex == index
            TapHandler {
                onTapped: {
                    if(tapCount <= 1) {
                        listView.currentIndex = (
                            listView.currentIndex == index? -1: index)
                        listView.refreshVisible()
                        print("Clicked" + initiative.height)
                    } else {
                        // Здесь открыть новую страницу - с текстом инициативы и
                        // комментариями к нему.
                    }
                }
            }
        }
    }
}
