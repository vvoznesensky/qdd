import QtQuick 2.14

Rectangle {
    id: rectangle
    width: parent.width
    height: childrenRect.height
    color: "lightsteelblue"

    property bool open

    signal sectionTapped(string sectionState)

    property var labels: {
        "discuss": "💬Обсуждение",
        "ballots": "🗳️Голосование",
        "results": "📊Результаты"}

    Text {
        id: theText
        text: labels[section]
        font.bold: true
        font.pixelSize: 20
    }
    Text {
        anchors.right: parent.right
        text: parent.open? "▼ ": "▲ "
    }
    TapHandler {
        onTapped: {
            rectangle.sectionTapped(section)
        }
    }
}

