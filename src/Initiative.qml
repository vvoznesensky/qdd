import QtQuick 2.12
import QtQuick.Controls 2.12 as Controls
import QtQuick.Layouts 1.12
import QtQuick.Shapes 1.14 as Shapes
import org.kde.kirigami 2.5 as Kirigami

Column {
    id: root
    property bool poll
    property bool current: false
    property var element
    property string state
    width: parent.width

    Row {
        width: parent.width

        ButtonColumn {
            id: leftColumn
            picture: root.element.face
            element: root.element
            current: root.current
        }
        Column {
            width: parent.width - (
                root.element.poll?rightColumn:leftColumn).width
            Controls.Label {
                id: heading
                visible: !root.current
                textFormat: Text.StyledText
                elide: Text.ElideRight
                wrapMode: Text.Wrap
                width: parent.width
                maximumLineCount: 2
                text: "<h3>" + root.element.headline
            }
            Row {
                visible: !root.current
                Text {
                    font.bold: root.element.chosen == 1
                    textFormat: Text.StyledText
                    text: ('👍<font color="green">' + root.element.cur_likes +
                        '</font>/' + root.element.all_likes)
                }
                Text {
                    font.bold: root.element.chosen == 2
                    textFormat: Text.StyledText
                    text: (' 🚧<font color="green">' + root.element.
                        cur_not_ready + '</font>/' + root.element.all_not_ready)
                }
                Text {
                    font.bold: root.element.chosen == 3
                    textFormat: Text.StyledText
                    text: (' 👎<font color="green">' + root.element.
                        cur_dislikes + '</font>/' + root.element.all_dislikes)
                }
                Text {
                    font.bold: root.element.chosen == 4
                    textFormat: Text.StyledText
                    text: (' 🗑<font color="green">' + root.element.
                        cur_trash_bins + '</font>/' + root.element.
                        all_trash_bins)
                }
            }
            Controls.Label {
                id: mainText
                visible: root.current
                textFormat: Text.StyledText
                wrapMode: Text.Wrap
                elide: Text.ElideRight
                width: parent.width
                height: root.height
                text: "<h3>" + root.element.headline + "</h3>" +
                    root.element.text
            }
        }
    }
    Shapes.Shape {
        width: parent.width
        height: 0
        Shapes.ShapePath {
            strokeWidth: 2
            strokeColor: "lightgray"
            startX: 0; startY: 0
            PathLine { x: width; y: 0 }
        }
    }
}
