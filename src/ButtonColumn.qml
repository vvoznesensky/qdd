import QtQuick 2.11
import QtQuick.Controls 2.11 as Controls
import org.kde.kirigami 2.5 as Kirigami

Column {
    id: root
    property alias picture: picButton.text
    property string headline
    property string body
    property bool current
    property var element
    width: picture.contentWidth
    Text {
        id: picButton
        font.pointSize: Kirigami.Units.gridUnit * (root.current? 3: 2)
    }
    RichButton {
        id: likesButton
        visible: root.current
        symbol: "👍"
        numerator: root.element.cur_likes
        denominator: root.element.all_likes
        onTapped: print("Thumb up tapped")
        chosen: root.element.chosen == 1
        width: picButton.contentWidth
    }
    RichButton {
        id: constructionButton
        visible: root.current
        symbol: '🚧'
        numerator: root.element.cur_not_ready
        denominator: root.element.all_not_ready
        onTapped: print("Not ready tapped")
        chosen: root.element.chosen == 2
        width: picButton.contentWidth
    }
    RichButton {
        id: dislikesButton
        visible: root.current
        symbol: "👎"
        numerator: root.element.cur_dislikes
        denominator: root.element.all_dislikes
        onTapped: print("Thumb down tapped")
        chosen: root.element.chosen == 3
        width: picButton.contentWidth
    }
    RichButton {
        id: trashBinButton
        visible: root.current
        symbol: "🗑" 
        numerator: root.element.cur_trash_bins
        denominator: root.element.all_trash_bins
        onTapped: print("Trash bin tapped")
        chosen: root.element.chosen == 4
        width: picButton.contentWidth
    }
}
